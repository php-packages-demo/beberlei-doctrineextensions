# beberlei-doctrineextensions

Add support for functions available in MySQL, Oracle, PostgreSQL and SQLite. https://packagist.org/packages/beberlei/doctrineextensions

## Unofficial documentation
* [*Symfony 4 and FULLTEXT search*](https://medium.com/@ikerib/symfony-4-and-fulltext-search-5600186c7cbf)
  2019-06 Iker Ibarguren